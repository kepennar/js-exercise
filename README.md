# Javascript exercises

Go to exercise-x branch

```
git checkout exercise-1
```

Install dependencies
```
npm install
```

Run tests
```
npm test
```

A test will fail. The exercise is to implement the missing function to make the test pass